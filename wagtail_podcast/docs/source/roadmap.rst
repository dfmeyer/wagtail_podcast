.. _roadmap-label:

==========================
Proposed Roadmap
==========================

0.1
   First release
0.1.5 (End of November)
   Have good test coverage
0.2 (1st week of December)
   Make template tags so that customising and using in own templates is easier
   Fully functional categories
   Seasons
   Automatic Ogg generation
0.3 (Mid-January)
   Video support with auto conversion (webm and mp4)
0.4 (End-January)
   Multiple podcasts per site
   Author filtering
   Tags and tag filtering
   Season filtering
0.5 (If demanded)
   Essentially a one click deploy version for people who want to host their own podcasts